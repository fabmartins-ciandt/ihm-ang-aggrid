import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatCardModule, MatButtonModule, MatIconModule } from '@angular/material';

import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  exports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    AgGridModule
  ]
})
export class SharedModule { }
