export interface OrderItem {
  market: string;
  station: string;
  order_no: number;
  start_date: Date;
  end_date: Date;
  product_code: string;
  priority_code: number;
  preemptible: string;
  advertiser_separation: string;
  revenue_type: string;
  break_type: string;
  spot_length: number;
}
