import { OrderItem } from './order-item.interface';

export interface Order {
  strata_parent_id: number;
  advertiser: string;
  estimate_no: number;
  status: string;
  items: OrderItem[];
}
