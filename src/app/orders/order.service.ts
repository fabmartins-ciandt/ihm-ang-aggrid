import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subscriber } from 'rxjs';

import { environment } from '../../environments/environment';

import { Order } from './order.interface';
import { OrderItem } from './order-item.interface';


@Injectable()
export class OrderService {
  private orders: Order[];

  constructor(private httpClient: HttpClient) {
    this.orders = [
      {
        strata_parent_id: 3689176, advertiser: 'Home Depot Inc', estimate_no: 76158, status: 'Pending', items: [
          {
            market: 'New York', station: 'KMCX-FM', order_no: 112723184, start_date: new Date('2018-09-01'),
            end_date: new Date('2018-09-10'), product_code: 'Best Spots', priority_code: 25, preemptible: 'No',
            advertiser_separation: '30 Mins', revenue_type: 'Local Agency Sales', break_type: 'Advertiser exclusivity', spot_length: 60
          },
          {
            market: 'Los Angeles', station: 'KMCX-FM', order_no: 112723184, start_date: new Date('2018-09-01'),
            end_date: new Date('2018-09-10'), product_code: 'Best Spots', priority_code: 25, preemptible: 'No',
            advertiser_separation: '30 Mins', revenue_type: 'Local Agency Sales', break_type: 'Advertiser exclusivity', spot_length: 60
          }
        ]
      },
      { strata_parent_id: 3689177, advertiser: 'Mc Donalds', estimate_no: 76159, status: 'Pending', items: [] },
      { strata_parent_id: 3689179, advertiser: 'Burger King', estimate_no: 76160, status: 'Pending', items: [] },
      { strata_parent_id: 3689180, advertiser: 'Subway', estimate_no: 76161, status: 'Pending', items: [] },
      { strata_parent_id: 3689181, advertiser: 'Toyota', estimate_no: 76162, status: 'Pending', items: [] }
    ];
  }

  public getOrders(): Observable<Order[]> {
    return new Observable<Order[]>((subscriber: Subscriber<Order[]>) => {
      if (this.orders.length > 0) {
        subscriber.next(this.orders);
      } else {
        this.httpClient.get<Order[]>(`${environment.apiUrl}/orders`).subscribe((orders: Order[]) => subscriber.next(orders),
          (error: any) => subscriber.error(error));
      }
    });
  }

  public getOrder(id: number): Observable<Order> {
    return new Observable<Order>((subscriber: Subscriber<Order>) => {
      if (this.orders.length > 0) {
        let order: Order;

        if (order = this.orders.filter((obj: Order) => obj.strata_parent_id === id).shift()) {
          subscriber.next(order);
        } else {
          subscriber.error('Not found.');
        }
      } else {
        this.httpClient.get<Order>(`${environment.apiUrl}/orders/${id}`).subscribe((order: Order) => subscriber.next(order),
          (error: any) => subscriber.error(error));
      }
    });
  }

  public patchOrder(id: number, order: Order) {
    return this.httpClient.patch<string>(`${environment.apiUrl}/orders/${id}`, order);
  }

  public patchOrderItems(id: number, orderItems: OrderItem[]) {
    return this.httpClient.patch<string>(`${environment.apiUrl}/orders/${id}/order-items`, orderItems);
  }
}
