import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

import { MatSnackBar } from '@angular/material';
import { GridOptions, CellValueChangedEvent } from 'ag-grid';
import { ValueGetterParams, TooltipParams } from 'ag-grid/dist/lib/entities/colDef';

import { Order } from '../order.interface';
import { OrderItem } from '../order-item.interface';

import { OrderService } from '../order.service';

import * as moment from 'moment';


@Component({
  selector: 'ihm-om-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
  public gridOptions: GridOptions;
  public order: Order;
  public orderItems: OrderItem[];

  constructor(private location: Location, private activatedRoute: ActivatedRoute, private matSnackBar: MatSnackBar,
    private orderService: OrderService) {
    this.gridOptions = {
      animateRows: true,
      columnDefs: [
        {
          headerName: 'Market', field: 'market', tooltipField: 'market', width: 134, cellEditor: 'agSelectCellEditor',
          cellEditorParams: { cellHeight: 18, values: ['New York', 'Los Angeles'] }
        },
        { headerName: 'Station', field: 'station', tooltipField: 'station', width: 134 },
        { headerName: 'Order No', field: 'order_no', tooltipField: 'order_no', width: 134 },
        {
          headerName: 'Start Date', field: 'start_date', tooltip: (tooltipParams: TooltipParams) => tooltipParams.value,
          width: 134, valueGetter: this.dateValueGetter
        },
        {
          headerName: 'End Date', field: 'end_date', tooltip: (tooltipParams: TooltipParams) => tooltipParams.value,
          width: 134, valueGetter: this.dateValueGetter
        },
        { headerName: 'Product Code', field: 'product_code', tooltipField: 'product_code', width: 145 },
        { headerName: 'Priority Code', field: 'priority_code', tooltipField: 'priority_code', width: 145 },
        { headerName: 'Preemptible', field: 'preemptible', tooltipField: 'preemptible', width: 150 },
        { headerName: 'Advertiser Separation', field: 'advertiser_separation', tooltipField: 'advertiser_separation', width: 200 },
        { headerName: 'Revenue Type', field: 'revenue_type', tooltipField: 'revenue_type', width: 150 },
        { headerName: 'Break Type', field: 'break_type', tooltipField: 'break_type', width: 134 },
        {
          headerName: 'Spot Length', field: 'spot_length', tooltipField: 'spot_length', width: 145, cellEditor: 'agSelectCellEditor',
          cellEditorParams: { cellHeight: 18, values: [10, 20, 30, 40, 50, 60] }
        }
      ],
      defaultColDef: {
        editable: true
      },
      enableFilter: true,
      enableSorting: true,
      enableColResize: true,
      gridAutoHeight: true,
      pagination: true,
      paginationPageSize: 7,
      rowHeight: 36,
      suppressDragLeaveHidesColumns: true
    };

    this.orderItems = [];
  }

  public ngOnInit(): void {
    this.orderService.getOrder(+this.activatedRoute.snapshot.params.id).subscribe((order: Order) => this.order = order);
  }

  public update(): void {
    this.orderItems = [];
    this.matSnackBar.open('Updated successfully.', null, { duration: 2000 });
  }

  public back(): void {
    this.location.back();
  }

  public submit(): void {
    this.matSnackBar.open('Submitted successfully.', null, { duration: 2000 });
    this.location.back();
  }

  public changed(cellValueChangedEvent: CellValueChangedEvent): void {
    if (cellValueChangedEvent.oldValue !== cellValueChangedEvent.newValue) {
      this.orderItems.push(cellValueChangedEvent.data);
    }
  }

  private dateValueGetter(valueGetterParams: ValueGetterParams) {
    return moment(valueGetterParams.data[valueGetterParams.column.getColId()]).format('M/D/YYYY');
  }
}
