import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ColDef, RowClickedEvent } from 'ag-grid';

import { Order } from '../order.interface';
import { OrderService } from '../order.service';

@Component({
  selector: 'ihm-om-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  public columnDefinitions: ColDef[];
  public orders: Order[];

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private orderService: OrderService) {
    this.columnDefinitions = [
      { headerName: 'Strata Parent ID', field: 'strata_parent_id' },
      { headerName: 'Advertiser', field: 'advertiser' },
      { headerName: 'Estimate No', field: 'estimate_no' },
      { headerName: 'Status', field: 'status' }
    ];
  }

  public ngOnInit(): void {
    this.orderService.getOrders().subscribe((orders: Order[]) => this.orders = orders);
  }

  public onRowClicked(rowClickedEvent: RowClickedEvent): void {
    this.router.navigate(['details', rowClickedEvent.data.strata_parent_id], { relativeTo: this.activatedRoute.parent });
  }
}
