import { Component } from '@angular/core';

@Component({
  selector: 'ihm-om-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent { }
