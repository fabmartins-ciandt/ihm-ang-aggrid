import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { MatSnackBarModule } from '@angular/material';
import { AgGridModule } from 'ag-grid-angular';
import { IhmHeaderModule, IhmSidenavModule } from '@ihm-software/ihm-ui-common';
import { AmChartsModule } from '@amcharts/amcharts3-angular';

import { CoreRoutingModule } from './core-routing.module';

import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './home/home.component';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatSnackBarModule,
    AgGridModule.withComponents([]),
    IhmHeaderModule,
    IhmSidenavModule,
    AmChartsModule,
    CoreRoutingModule
  ],
  declarations: [LayoutComponent, HomeComponent],
  exports: [
    LayoutComponent
  ]
})
export class CoreModule { }
