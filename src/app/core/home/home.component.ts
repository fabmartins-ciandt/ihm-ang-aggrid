import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { AmChartsService, AmChart } from '@amcharts/amcharts3-angular';

@Component({
  selector: 'ihm-om-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit, OnDestroy {

  private chart: AmChart;

  constructor(private amChartsService: AmChartsService) { }

  public ngAfterViewInit(): void {
    this.chart = this.amChartsService.makeChart('chart', {
      'type': 'serial',
      'theme': 'light',
      'marginRight': 70,
      'dataProvider': [{
        'market': 'New York',
        'orders': 3025,
        'color': '#FF0F00'
      }, {
        'market': 'Los Angeles',
        'orders': 1882,
        'color': '#FF6600'
      }, {
        'market': 'Houston',
        'orders': 1809,
        'color': '#FF9E01'
      }, {
        'market': 'San Antonio',
        'orders': 1322,
        'color': '#FCD202'
      }, {
        'market': 'San Fracisco',
        'orders': 1122,
        'color': '#F8FF01'
      }, {
        'market': 'Philadelphia',
        'orders': 1114,
        'color': '#B0DE09'
      }, {
        'market': 'Chicago',
        'orders': 984,
        'color': '#04D215'
      }, {
        'market': 'Austin',
        'orders': 711,
        'color': '#0D8ECF'
      }, {
        'market': 'Boston',
        'orders': 665,
        'color': '#0D52D1'
      }],
      'valueAxes': [{
        'axisAlpha': 0,
        'position': 'left',
        'title': 'Orders per Market'
      }],
      'startDuration': 1,
      'graphs': [{
        'balloonText': '<b>[[category]]: [[value]]</b>',
        'fillColorsField': 'color',
        'fillAlphas': 0.9,
        'lineAlpha': 0.2,
        'type': 'column',
        'valueField': 'orders'
      }],
      'chartCursor': {
        'categoryBalloonEnabled': false,
        'cursorAlpha': 0,
        'zoomable': false
      },
      'categoryField': 'market',
      'categoryAxis': {
        'gridPosition': 'start',
        'labelRotation': 45
      },
      'export': {
        'enabled': true
      }
    });
  }

  public ngOnDestroy(): void {
    if (this.chart) {
      this.amChartsService.destroyChart(this.chart);
    }
  }
}
