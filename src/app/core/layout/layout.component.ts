import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';

import { IhmSidenavInterface } from '@ihm-software/ihm-ui-common/src/app/components/ihm-sidenav/ihm-sidenav.interface';

@Component({
  selector: 'ihm-om-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  public username: string;
  public links: IhmSidenavInterface[];

  constructor(private router: Router) {
    this.username = 'Fabio Martins';
    this.links = [
      { label: 'Home', icon: 'home', link: '/home', active: false },
      { label: 'Orders', icon: 'description', link: '/orders', active: false }
    ];
  }

  public ngOnInit(): void {
    this.router.events.subscribe((event: RouterEvent) => {
      if (event instanceof NavigationEnd) {
        this.links.map((link: IhmSidenavInterface) => {
          link.active = (event.url.startsWith(link.link));
          return link;
        });
      }
    });
  }
}
